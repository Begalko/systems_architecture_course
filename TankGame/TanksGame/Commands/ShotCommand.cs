﻿using Utils.Numeric;
using TanksGame.Adapters;

namespace TanksGame.Commands
{
    public class ShotCommand : ICommand
    {
        public void Execute()
        {
            new MoveCommand(new MovableAdapter().Execute());
        }
    }
}
