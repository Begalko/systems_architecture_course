﻿using TanksGame.Contracts;
using Utils.Numeric;

namespace TanksGame.Commands
{
    public class RotateCommand : ICommand
    {
        private IMoveRotatable moveRotatable;

        public RotateMacroCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            new RotateSelfCommand(moveRotatable).Execute();
            new RotateVelocityCommand(moveRotatable).Execute();
        }
    }


    public class StartRotateCommand
    {
        private IMoveRotatable moveRotatable;
        public StartRotateCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            var rotateMacroCommand = new RotateMacroCommand(moveRotatable);
            IoC.Resolve<object>("queue.add", rotateMacroCommand);
            moveRotatable.SetCommand(rotateMacroCommand);
        }
    }


    public class RotateSelfCommand : ICommand
    {
        private IRotatable rotatable;

        public RotateSelfCommand(IRotatable rotatable)
        {
            this.rotatable = rotatable;
        }

        public void Execute()
        {
            var currentDirection = rotatable.Direction;
            rotatable.Direction = new Vector(
                (int)Math.Round(currentDirection.GetNComponent(1) * Math.Cos(rotatable.AngularVelocity.ToRadians())) 
                - (int)Math.Round(currentDirection.GetNComponent(2) * Math.Sin(rotatable.AngularVelocity.ToRadians())),
                (int)Math.Round(currentDirection.GetNComponent(2) * Math.Cos(rotatable.AngularVelocity.ToRadians()))
                + (int)Math.Round(currentDirection.GetNComponent(1) * Math.Sin(rotatable.AngularVelocity.ToRadians()))
            );
        }
    }


    public class StopRotateCommand : ICommand
    {
        private IMoveRotatable moveRotatable;
        public StopRotateCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            moveRotatable.GetCommand();
            moveRotatable.RemoveCurrentCommand();
        }
    }

    public class RotateVelocityCommand : ICommand
    {
        IMoveRotatable moveRotateable;

        public RotateVelocityCommand(IMoveRotatable moveRotateable)
        {
            this.moveRotateable = moveRotateable;
        }

        public void Execute()
        {
            var currentVelocity = moveRotateable.Velocity;
            moveRotateable.Velocity = new Vector(
                (int)Math.Round(currentVelocity.GetNComponent(1) * Math.Cos(moveRotateable.AngularVelocity.ToRadians()))
                - (int)Math.Round(currentVelocity.GetNComponent(2) * Math.Sin(moveRotateable.AngularVelocity.ToRadians())),
                (int)Math.Round(currentVelocity.GetNComponent(2) * Math.Cos(moveRotateable.AngularVelocity.ToRadians()))
                + (int)Math.Round(currentVelocity.GetNComponent(1) * Math.Sin(moveRotateable.AngularVelocity.ToRadians()))
            );
            IoC.Resolve<object>("queue.add", this);
        }
    }

}
