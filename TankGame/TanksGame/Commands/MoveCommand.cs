﻿using TanksGame.Contracts;
using Utils.Numeric;
using TanksGame.Adapters;

namespace TanksGame.Commands
{
    public class MoveCommand : ICommand
    {
        private IMovable movable;

        public MoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            movable.Position += movable.Velocity;
        }
    }

    public class StartMoveCommand : ICommand
    {
        private IMovable movable;
        public StartMoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            var moveCommand = new MoveCommand(movable);
            movable.SetCommand(moveCommand);
        }
    }

    public class StopMoveCommand : ICommand
    {
        private IMovable movable;
        public StopMoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            movable.RemoveCurrentCommand();
        }
    }
}
