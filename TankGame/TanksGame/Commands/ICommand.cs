﻿namespace TanksGame.Commands
{
    public interface ICommand
    {
        public void Execute();
    }
}
