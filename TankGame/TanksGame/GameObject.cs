﻿namespace TanksGame
{
    public interface IGameObject
    {
        public object Get(string name);
        public void Set(string name, object instance);
        public void Remove (string name);
    }
}
