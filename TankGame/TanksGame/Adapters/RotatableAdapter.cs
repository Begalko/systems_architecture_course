﻿using TanksGame.Contracts;
using Utils.Numeric;

namespace TanksGame.Adapters
{
    public class RotatableAdapter : IRotatable
    {
        private readonly IGameObject _instance;

        public RotatableAdapter(IGameObject instance)
        {
            _instance = instance;
        }

        public Vector Direction
        {
            get => (Vector)_instance.Get("Direction");
            set => _instance.Set("Direction", value);
        }

        public Vector AngularVelocity
        {
            get => (Vector)_instance.Get("AngularVelocity");
            set => _instance.Set("AngularVelocity", value);
        }

        public ICommand GetCommand()
        {
            return (ICommand)_instance.Get("rotateCommand");
        }

        public void RemoveCurrentCommand()
        {
            _instance.Remove("rotateCommand");
        }

        public void SetCommand(ICommand command)
        {
            _instance.Set("rotateCommand", command);
        }

    }
}
