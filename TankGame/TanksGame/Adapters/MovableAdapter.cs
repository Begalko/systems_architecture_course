﻿using TanksGame.Contracts;
using Utils.Numeric;

namespace TanksGame.Adapters
{
    public class MovableAdapter : IMovable
    {
        private readonly IGameObject _instance;

        public MovableAdapter(IGameObject instance)
        {
            _instance = instance;
        }

        public Vector Position
        {
            get => (Vector)_instance.Get("Position");
            set => _instance.Set("Position", value);
        }

        public Vector Velocity
        {
            get => (Vector)_instance.Get("Velocity");
            set => _instance.Set("Velocity", value);
        }

        public ICommand GetCommand()
        {
            return (ICommand)_instance.Get("moveCommand");
        }

        public void RemoveCurrentCommand()
        {
            _instance.Remove("moveCommand");
        }

        public void SetCommand(ICommand command)
        {
            _instance.Set("moveCommand", command);
        }

    }
}
