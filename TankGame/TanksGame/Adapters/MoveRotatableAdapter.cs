﻿using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;
using Utils.Numeric;

namespace TanksGame.Adapters
{
    internal class MoveRotatableAdapter : IMoveRotatable
    {
        private readonly IGameObject _instance;

        public MoveRotatableAdapter(IGameObject instance)
        {
            _instance = instance;
        }

        public Vector Direction {
            get => (Vector)_instance.Get("Direction");
            set => _instance.Set("Direction", value);
        }
        public int AngularVelocity {
            get => (int)_instance.Get("AngularVelocity");
            set => _instance.Set("AngularVelocity", value);
        }
        public Vector Position {
            get => (Vector)_instance.Get("Position");
            set => _instance.Set("Position", value);
        }
        public Vector Velocity {
            get => (Vector)_instance.Get("Velocity");
            set => _instance.Set("Velocity", value);
        }

        public ICommand GetCommand()
        {
            return (ICommand)_instance.Get("moveRotateCommand");
        }

        public void RemoveCurrentCommand()
        {
            _instance.Remove("moveRotateCommand");
        }

        public void SetCommand(ICommand command)
        {
            _instance.Set("moveRotateCommand", command);
        }
    }
}
