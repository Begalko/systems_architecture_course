﻿using Utils.Numeric;

namespace TanksGame.Contracts
{
    public interface IRotatable
    {
        Vector Direction { get;}
        Vector AngularVelocity { get;}
    }
}
