﻿using Utils.Numeric;

namespace TanksGame.Contracts
{
    public interface IMovable
    {
        Vector Position { get;}
        Vector Velocity { get;}
    }
}
