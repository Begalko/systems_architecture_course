﻿using Utils.Numeric;

namespace TanksGame.Contracts
{
    public interface IMoveRotateable
    {
        Vector Direction { get;}
        Vector AngularVelocity { get;}
        Vector Velocity { get;}
    }
}
