﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TanksGame.Commands;
using TanksGame.Contracts;
using System;
using Utils.Numeric;

namespace TanksGameTests.Commands
{
    [TestClass()]
    public class RotateCommandTests
    {
        private void LetObjectHasDirection(Mock<IMoveRotateable> mock, Vector vector)
        {
            mock.SetupGet(m => m.Direction).Returns(vector).Verifiable();
        }

        private void LetObjectHasAngularVelocity(Mock<IMoveRotateable> mock, Vector vector)
        {
            mock.SetupGet(m => m.AngularVelocity).Returns(vector).Verifiable();
        }

        private void ThenObjectMustBeRotatedToDirection(Mock<IMoveRotateable> mock, Vector vector)
        {
            mock.SetupSet(m => m.Direction = It.Is<Vector>(v => Vector.AreEquals(v, vector))).Verifiable();
        }

        private void LetThrowsExceptionIfCantGetDirection(Mock<IMoveRotateable> mock)
        {
            mock.SetupGet(m => m.Direction).Throws<Exception>();
        }

        private void LetThrowsExceptionIfCantGetAngularVelocity(Mock<IMoveRotateable> mock)
        {
            mock.SetupGet(m => m.AngularVelocity).Throws<Exception>();
        }

        private void LetThrowsExceptionIfCantSetDirection(Mock<IMoveRotateable> mock, Vector vector)
        {
            mock.SetupSet(m => m.Direction = It.IsAny<Vector>()).Throws<Exception>();
        }

        private static void ExceptionShouldBeThrew<T>(Action action) where T : Exception
        {
            Assert.ThrowsException<T>(action);
        }

        private static void AllActionsWithMockShouldBePerformed(Mock<IMoveRotateable> mock)
        {
            mock.VerifyAll();
        }

        [TestMethod()]
        public void RotateCommandShouldMoveObject()
        {
            var mock = new Mock<IMoveRotateable>();
            LetObjectHasDirection(mock, new Vector(3, 8));
            LetObjectHasAngularVelocity(mock, new Vector(2, -3));
            ThenObjectMustBeRotatedToDirection(mock, new Vector(5, 5));
            new RotateSelfComand(mock.Object).Execute();
            AllActionsWithMockShouldBePerformed(mock);
        }

        [TestMethod()]
        public void RotateCommandExceptionIfCantGetDirection()
        {
            var mock = new Mock<IMoveRotateable>();
            LetThrowsExceptionIfCantGetDirection(mock);
            ExceptionShouldBeThrew<Exception>(() => new RotateSelfComand(mock.Object).Execute());
        }

        [TestMethod()]
        public void RotateCommandExceptionIfCantGetAngularVelocity()
        {
            var mock = new Mock<IMoveRotateable>();
            LetThrowsExceptionIfCantGetAngularVelocity(mock);
            ExceptionShouldBeThrew<Exception>(() => new RotateSelfComand(mock.Object).Execute());
        }

        [TestMethod()]
        public void RotateCommandExceptionIfCantSetDirection()
        {
            var mock = new Mock<IMoveRotateable>();
            LetObjectHasDirection(mock, new Vector(3, 8));
            LetObjectHasAngularVelocity(mock, new Vector(2, -3));
            LetThrowsExceptionIfCantSetDirection(mock, new Vector(3, 4));
            ExceptionShouldBeThrew<Exception>(() => new RotateSelfComand(mock.Object).Execute());
        }
    }
}
