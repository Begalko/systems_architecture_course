﻿using System;
using System.Linq;

namespace Utils.Numeric
{
        public class Vector
    {
        private int[] components;

        public Vector(params int[] components)
        {
            this.components = components;
        }

        private static bool IsSameSize(Vector first, Vector second)
        {
            return first.components.Length == second.components.Length;
        }

        public static Vector Sum (Vector first, Vector second)
        {
            if (!IsSameSize(first, second))
            {
                throw new ArgumentException("Различный размер векторов");
            }

            return new Vector(first.components.Zip(second.components,
                (fromFirst, fromSecond) => fromFirst + fromSecond).ToArray());
        }

        public static Vector operator + (Vector first, Vector second)
        {
            return Sum(first, second);
        }

        public static bool AreEquals(Vector first, Vector second)
        {
            return Enumerable.SequenceEqual(first.components, second.components);
        }
    }
}
