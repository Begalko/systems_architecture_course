using Xunit;
using Prime.Services;
using System;

namespace Prime.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {
        [Fact]
        public void IsPrime_InputIs1_ReturnFalse()
        {
           
            var primeService = new PrimeService();
            var result = primeService.IsPrime(0.5);
            Assert.True(result, "Test complite");
        }
    }
}